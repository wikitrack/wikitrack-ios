# README #

A little handy tool for watching the changes in your favourite wiki projects. For now, we have three features.
 
1. Recent Changes
2. User Contributions 
3. Watchlist

The above module works with the diffs module.

### What is this repository for? ###

An app for tracking the wiki projects and wiki users. *Ver 0.1

### Used Library ###
* AFNetworking
http://afnetworking.com/
* SwiftyJSON
https://github.com/SwiftyJSON/SwiftyJSON
* DiffMatchPatch iOS
https://github.com/Blahah/iOS-DiffMatchPatch