import UIKit

class languageController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var langlist: UITableView!
    @IBOutlet var langFilter: UITextField!
    var kCellIdentifier: String = "LanguageCell"
    var codeForProj: NSString = ".wikipedia"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchString = ""
//        self.langlist.separatorStyle = UITableViewCellSeparatorStyle.None
        languageParser()
        //langFilter.addTarget(self, action: Selector(textFieldDidChange(langFilter)), forControlEvents: UIControlEvents.EditingChanged)
//        canArr.removeAllObjects()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func detectChange(sender: UITextField) {
        println(sender.text)
        searchString = sender.text
        reloadTableDataFiltered()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        langFilter.resignFirstResponder() //The keyboard is dismissed when the user hits enter.
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        println("User has opted to use the search feature. And is going to enter text(searchString)")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    
        return canArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as UITableViewCell
        //let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "menuCell")
        cell.textLabel?.text =  nameArr[indexPath.row] as NSString
        cell.detailTextLabel?.text = canArr[indexPath.row] as NSString
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //performSegueWithIdentifier("segue.push.menu", sender: nil)
    }

    
    /* The language parser function is used to fetch the languages.json file(stored within the project). The SwiftyJSON library is then used to parse this json.
    The canonical_name, language code and the local name are stored in 3 different arrays. The table is used to display the canonical and the local names. The language code is used at a later stage.
    Language Sorting has been done using this page: http://wikimediafoundation.org/wiki/Special:SiteMatrix
    */
    func languageParser()
    {
        clearArrayObjects()
        self.langlist.reloadData() // Clearing all objects from the array so as to not overpopulate the table with unwanted data. This was observed when user switches between projects.
        if (langListReady == false)
        {
            println(selProjName) //Using if condition to display languages that are supported by the project.
            
            
            if ( selProjName == projArr[0].description ) //WIKIPEDIA
            {
                
                readJSONfromFile()
                populateArrays()
              /*  if let file = NSBundle(forClass:languageController.self).pathForResource("languages", ofType:"json") // The language.json file is fetched and stored in a constant named 'file' using NSBundle type. If condition is used to detect whether the file is found.
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSMutableArray
                }
                   langJson = JSON(data: data) //As NSData is the parameter type that the JSON type variable langJson will accept.
                }
                for (key: String, subJson: JSON) in langJson //For every array element in 'langJson', the dictionary items are identified and stored in respective MutableArrays.
                {
                    //println("Key: \(key)")
                    //println(subJson["name"])
                    canonicalTemp = subJson["canonical_name"]
                    codeTemp = subJson["code"]
                    nameTemp = subJson["name"]
                    canArr.addObject(canonicalTemp.description) //Description is a member of the JSON data type that is used when we need to assign a value to NSString.
                    codeArr.addObject(codeTemp.description)
                    nameArr.addObject(nameTemp.description)
                }
*/
            }
            else if ( selProjName == projArr[1].description ) //WIKTIONARY
            {

                if let file = NSBundle(forClass:languageController.self).pathForResource("wiktionary_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
/*                if let file = NSBundle(forClass: languageController.self).pathForResource("wiktionary_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file)
                    langJson = JSON(data:data)
                }
                for (key: String, subJson: JSON) in langJson["resources"]
                {
                    //println(subJson.arrayValue.count)
                    //println(subJson[0]["item"].arrayValue.count)
                    //println(subJson[0]["item"][0])
                    var index = 0
                    var codeIndex = 0
                    for index in index..<subJson.arrayValue.count
                    {
                        for codeIndex in codeIndex..<subJson[index]["item"].arrayValue.count
                        {
                            if ( index == 0 )
                            {
                                codeTemp = subJson[index]["item"][codeIndex]
                                //println(codeTemp)
                                codeArr.addObject(codeTemp.description)
                                //println(codeArr)
                            }
                            else if ( index == 1 )
                            {
                                nameTemp  = subJson[index]["item"][codeIndex]
                                nameArr.addObject(nameTemp.description)
                                //println(nameTemp)
                                
                            }
                            else
                            {
                                canonicalTemp = subJson[index]["item"][codeIndex]
                                canArr.addObject(canonicalTemp.description)
                                //println(canonicalTemp)
                            }
                        }
                    }
                }
*/
            }
            else if ( selProjName == projArr[2].description ) //WIKIQUOTE
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikiquote_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[3].description ) //WIKIBOOKS
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikibooks_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[4].description ) //WIKISOURCE
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikisource_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[5].description ) //WIKINEWS
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikinews_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[6].description ) //WIKIVERSITY
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikiversity_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[7].description ) //WIKIVOYAGE
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("wikivoyage_languages", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
            }
            else if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description) //COMMONS, META-WIKI, INCUBATOR, WIKISPECIES
            {
                if let file = NSBundle(forClass:languageController.self).pathForResource("one_language", ofType:"json")
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
                }
                println(languageData.count)
                
                populateArrays()
                
/*                if let file = NSBundle(forClass:languageController.self).pathForResource("one_language", ofType:"json") // The language.json file is fetched and stored in a constant named 'file' using NSBundle type. If condition is used to detect whether the file is found.
                {
                    let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
                    langJson = JSON(data: data) //As NSData is the parameter type that the JSON type variable langJson will accept.
                }
                for (key: String, subJson: JSON) in langJson //For every array element in 'langJson', the dictionary items are identified and stored in respective MutableArrays.
                {
                    //println("Key: \(key)")
                    //println(subJson["name"])
                    canonicalTemp = subJson["canonical_name"]
                    codeTemp = subJson["code"]
                    nameTemp = subJson["name"]
                    canArr.addObject(canonicalTemp.description) //Description is a member of the JSON data type that is used when we need to assign a value to NSString.
                    codeArr.addObject(codeTemp.description)
                    nameArr.addObject(nameTemp.description)
                }
*/
                // Adding an alertView here to inform the user that only one language is available for selection.
                var alert: UIAlertView = UIAlertView()
                var alTitle = "NOTE"
                var alMessage = "This project currently supports only one langugage."
                alert.title = alTitle
                alert.message = alMessage
                alert.addButtonWithTitle("OK")
                alert.show()
                //performSegueWithIdentifier("segue.push.menu", sender: nil)
            }
//            else if ( selProjName == projArr[9] as NSString ) //META-WIKI
//            {
//                
//            }
            
            
            
            dispatch_async(dispatch_get_main_queue(), {
                self.langlist!.reloadData() //The tableView is reloaded and the dispatch_async is used to bring the process from background to foreground.
            })
            
            langListReady = true
        }
        println(canArr.count)
        //projectCode = codeForProj
        //println(projectCode)
        
    }
    
    func reloadTableDataFiltered()
    {
        if ( searchString.length == 0 )
        {
            clearArrayObjects()
            populateArrays()
            self.langlist!.reloadData()
        }
        else
        {
            var filterArray: NSMutableArray = []
            filterArray.removeAllObjects()
            filterArray = languageData.mutableCopy() as NSMutableArray
            println("LanguageData:\(languageData.count)")
            println("FilterArray:\(filterArray.count)")
            var filterPredicate: NSPredicate = NSPredicate(format: "SELF.name contains[c] %@ || SELF.canonical_name contains[c] %@ || SELF.code == [c] %@", searchString, searchString, searchString)!
            filterArray.filterUsingPredicate(filterPredicate)
            clearArrayObjects()
            populateMutableArrays(filterArray)
            println(codeArr.count)
            self.langlist!.reloadData()
        }
    }
    
    
    func readJSONfromFile()
    {
        if let file = NSBundle(forClass:languageController.self).pathForResource("languages", ofType:"json") // The language.json file is fetched and stored in a constant named 'file' using NSBundle type. If condition is used to detect whether the file is found.
        {
            let data = NSData(contentsOfFile: file) //Contents of the 'file' are then stored in the form of NSData.
            languageData = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
        }
    }
    
    func populateArrays()
    {
        var dictionary: NSDictionary = [:]
        for dictionary in languageData
        {
            if let name = dictionary["name"] as? NSString {  nameArr.addObject(name)  }
            if let code = dictionary["code"] as? NSString {  codeArr.addObject(code)  }
            if let can_name = dictionary["canonical_name"] as? NSString {  canArr.addObject(can_name)  }
        }
    }
    
    func populateMutableArrays(array: NSMutableArray)
    {
        var dictionary: NSDictionary = [:]
        for dictionary in array
        {
            if let name = dictionary["name"] as? NSString {  nameArr.addObject(name)  }
            if let code = dictionary["code"] as? NSString {  codeArr.addObject(code)  }
            if let can_name = dictionary["canonical_name"] as? NSString {  canArr.addObject(can_name)  }
        }
    }
    
    func clearArrayObjects()
    {
        canArr.removeAllObjects()
        codeArr.removeAllObjects()
        nameArr.removeAllObjects()
    }
    

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        var menuController: menuPage = segue.destinationViewController as menuPage // segue parameter is being passed with a member destinationViewController to direct the app the menuPage.
        var tableIndex = langlist!.indexPathForSelectedRow()!.row  // The row selected by the user is identified using the indexPathForSelectedRow of the table.
        var tempCode = codeArr[tableIndex] as NSString // This index number can then be passed to the CodeArray to idetify the language code that we'll be using in our next view.
        menuController.languageCode = tempCode // The identified language code is assigned to a variable from the menuPage so that it can be used there.
    }
    

}