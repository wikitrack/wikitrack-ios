import UIKit


class LoginViewController: UIViewController, SuccessProtocol
{
    var LM = loginModule?()
    //var menuObj = menuPage()
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    //var loginDelegate: loginProtocol?
    //var mVC: menuPage = menuPage()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        LM = loginModule(delegate: self)
        // Do any additional setup after loading the view, typically from a nib.
        usernameField.addTarget(self, action: Selector(textFieldDidChange()), forControlEvents: UIControlEvents.EditingChanged)
        passwordField.addTarget(self, action: Selector(textFieldDidChange()), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(Bool)
    {
        super.viewDidAppear(true)
        removeCookies()
        usernameField.becomeFirstResponder()
    }
    
        
    func textFieldDidChange()
    {
        textEntered = ((usernameField.text.isEmpty == false) && (passwordField.text.isEmpty == false)) ? true : false
    }
    
    func textFieldShouldReturn( textField: UITextField )  -> Bool
    {
        if ( textField == usernameField )
        {
            passwordField.becomeFirstResponder()
            println("Step No. 1 complete.")
        }
        else if ( textField == passwordField )
        {
            lgname = usernameField.text
            lgpassword = passwordField.text
            //self.dismissViewControllerAnimated(true, completion: nil)
            //menuObj.lModule?.loginRequest()
            LM!.loginRequest()
            //loginTokenOp()
        }
        return true
    }
    
    override func viewDidDisappear(Bool)
    {
        if ( loggedIN == true )
        {
            //NSNotificationCenter.defaultCenter().postNotificationName("LoginSucess", object: nil) //Firing a notification when log in is successful
            //loginDelegate?.handleLogin!()
        }
    }
    
    func manualSegue()
    {
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var navMenu: menuPage = storyboard.instantiateViewControllerWithIdentifier("menuID") as menuPage
        self.presentViewController(navMenu, animated: true, completion: nil)
        
    }
    
    func handleLogin()
    {
        //println("I wish not to be used.")
        navigationController?.popViewControllerAnimated(true)
    }
    
    func removeCookies() //Deleting cookies
    {
        var cookieUser: NSHTTPCookie
        var cookieJar: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookieUser in cookieJar.cookies!
        {
            cookieJar.deleteCookie(cookieUser as NSHTTPCookie)
        }
        
        /*attemptAtLg = false
        loggedIN = false //Changing the boolean condition to false. Note: This may be done near logout section as well. Also if this method is used for anything but the logout section, this part may need to be removed. */
    }
    
}