import UIKit

class menuPage: UIViewController, UITableViewDelegate, UITableViewDataSource { // This is an interesting read about ViewController initialization: http://stackoverflow.com/questions/2053498/order-of-uiviewcontroller-initialization-and-loading
    
    @IBOutlet var menuTable: UITableView?
    let kCellIdentifier: String = "TitleCell"
    var languageCode: NSString = "en"
    //var wlPOPobj = rcAPIcall?()
    //var loginObject: LoginViewController = LoginViewController()
    //var lModule = loginModule?()
    
    @IBAction func navBack(sender: AnyObject) {
        // navigationController?.popToRootViewControllerAnimated(true)//(ProjectController(), animated: true)
        performSegueWithIdentifier("unwindWelcome", sender: nil)
    }
    
//    func transitionToWelcome() {
//        var backBarButton = UIBarButtonItem(title: "Home", style: UIBarButtonItemStyle.Bordered, target: self, action: "takeMeToWelcome:")
//        menuPage().navigationItem.backBarButtonItem = backBarButton
//    }
//    
//    func takeMeToWelcome(sender: AnyObject) {
//        performSegueWithIdentifier("unwindWelcome", sender: sender)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //wlPOPobj = rcAPIcall(delegate: self)
        //loginObject.loginDelegate = self
        //lModule = loginModule(delegate: self)
        //loadMenu()
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector(handleLogin()), name: "LoginSuccess", object: nil)  //Subscribe to the Notification that will be sent with the 'name' mentioned in the parameter.
        println("Menu Page Loaded")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(Bool)
    {
        println("\n\nUpdating Table\n\n")
//        dispatch_async(dispatch_get_main_queue(), {
//            self.menuTable!.reloadData()
//        })
        loadMenu()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return menuArray.count - One
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as UITableViewCell
        //let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "menuCell")
        cell.textLabel?.text =  menuArray[indexPath.row] as NSString
        cell.imageView?.image = UIImage(named: imgArr[indexPath.row] as NSString)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var index = menuTable!.indexPathForSelectedRow()!.row
        recordValue = menuArray[index].description
        
//            if ( recordValue == menuArray[2].description )
//        
//            {
//
//                var alert: UIAlertView = UIAlertView()
//                var alTitle = "Under Construction"
//                var alMessage = "This feature is currently being worked on and will be available in the next release. Thank you for your patience."
//                alert.title = alTitle
//                alert.message = alMessage
//                alert.addButtonWithTitle("OK")
//                alert.show()
//               // navigationController?.popToRootViewControllerAnimated(true)
//        }
        
        if ( recordValue == menuArray[3].description )
            {
                if ( LI == menuArray[4].description )
                {
                    println("You can logout now!")
                    logout()
                    removeCookies()
                    loadMenu()
                    alertMessage()
                }
                else
                {
                    performSegueWithIdentifier("segue.push.login", sender: nil)
                }
            }
        else if ( recordValue == menuArray[0].description || recordValue == menuArray[2].description )
            {
                performSegueWithIdentifier("segue.push.rc", sender: nil)
            }
        else if ( recordValue == menuArray[1].description )
        {
            performSegueWithIdentifier("segueSELuser", sender: nil)
        }

    }
    
    func loadMenu()
    {
        var tempArray: NSMutableArray = []
        if let file = NSBundle(forClass:menuPage.self).pathForResource("menuItems", ofType:"json")
        {
            let data = NSData(contentsOfFile: file)
            menuJson = JSON(data: data!)
        }
        for (key: String, subJson: JSON) in menuJson["menuItem"]
        {
            println("Key: \(key)")
            var index = 1
            for index in index...5
            {
                tempMenu = subJson["\(index)"]
                tempArray.addObject(tempMenu.description)
            }
            if loggedIN //Changing the label of the tableView on a successful login. Alternatives: http://stackoverflow.com/questions/20731838/reloading-tableview-of-one-view-controller-from-a-pushed-view-controller
            {
                menuArray = tempArray
                menuArray.exchangeObjectAtIndex(Th, withObjectAtIndex: Fo)
//                menuArray.removeObjectAtIndex(3)
                println(menuArray)
            }
            else
            {
            menuArray = tempArray
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.menuTable!.reloadData()
            })
        }
        lCode = languageCode
        println(lCode)
    }
    
    
    func manualSegue()
    {
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var navToLogin: LoginViewController = storyboard.instantiateViewControllerWithIdentifier("Login") as LoginViewController
        self.presentViewController(navToLogin, animated: true, completion: nil)
    }

    
    /*    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
    //        if (segue.identifier == "segue.push.rc")
    //        {
    var tableIndex = menuTable!.indexPathForSelectedRow()!.row
    var selectedMenu: NSString = menuArray[tableIndex] as NSString
    if ( selectedMenu == "Recent Changes" )
    {
    var viewController: ViewController = segue.destinationViewController as ViewController
    //viewController.inNum = selectedMenu
    //viewController.codeForURL = languageCode
    //rcAPI.lcode = languageCode
    }
    else if ( selectedMenu == "User Contributions" || selectedMenu == "Watch List")
    {
    //self.performSegueWithIdentifier("segue.push.menu", sender: self)
    var alert: UIAlertView = UIAlertView()
    var alTitle = "Under Construction"
    var alMessage = "This feature is currently being worked on and will be available in the next release. Thank you for your patience."
    alert.title = alTitle
    alert.message = alMessage
    alert.addButtonWithTitle("OK")
    alert.show()
    
    }
    }
    */
    
    
    func logout()
    {
        var url: NSURL = NSURL (string: logoutURL)!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description ) //Commons, Metawiki, Incubator, Wikispecies do not need the language code in their API request URL.
        {
            url = NSURL( string: http + projectCode + logoutURL )!
        }
        else
        {
            url = NSURL( string: http + lCode + projectCode + logoutURL )!
        }
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: { data, response, error in
            let urlData = NSData(contentsOfURL: url)
            let json = JSON(data: urlData!)
            println(json)
            
//            var tempCookie: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
//            var cookie: NSArray = tempCookie.cookies!
//            var tempHeader: NSDictionary = NSHTTPCookie.requestHeaderFieldsWithCookies(cookie)
//            
//            println(" \nCookie: \(cookie)")
//            println(" \ntempHeader \(tempHeader)")
            
            
            
        })
        task.resume()
    }
    
    func removeCookies() //Deleting cookies
    {
        var cookieUser: NSHTTPCookie
        var cookieJar: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookieUser in cookieJar.cookies!
        {
            cookieJar.deleteCookie(cookieUser as NSHTTPCookie)
        }
        
        attemptAtLg = false
        loggedIN = false //Changing the boolean condition to false. Note: This may be done near logout section as well. Also if this method is used for anything but the logout section, this part may need to be removed.
        
        // Help related to cookies: http://stackoverflow.com/questions/21165633/objective-c-send-cookies-to-uiwebview
        //http://stackoverflow.com/questions/4471629/how-to-delete-all-cookies-of-uiwebview
    }
    
    
    func alertMessage()
    {
        var alert = UIAlertView()
        var alTitle = "Done"
        var alMessage: NSString = lgname + " has been logged out successfully."
        alert.title = alTitle
        alert.message = alMessage
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    func updateTable()
    {
        println("************************  \(loggedIN)  ***************")
        //        if ( loggedIN == true )
        //        {
        //            var row: NSInteger = 2
        //            var section: NSInteger = 0
        //            var indexPathNEW: NSIndexPath = NSIndexPath(forRow: row, inSection: section)
        //            var path: [NSIndexPath] = [indexPathNEW]
        println("-----------------------------------Hey------------------------------------")
        //            dismissViewControllerAnimated(true, completion: nil)
        //viewDidLoad()
        //var NEWcell: UITableViewCell = tableView(menuTable, cellForRowAtIndexPath: indexPathNEW)
        //NEWcell.textLabel?.text = "Log out"
        //menuTable!.reloadData()
        
        
        //menuTable.reloadRowsAtIndexPaths(path, withRowAnimation: UITableViewRowAnimation.Fade)
        println("Done.")
        //        }
    }

    
    
    
}