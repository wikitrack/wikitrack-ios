import UIKit

class ProjectController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var ProjTable: UITableView!
    var kCellIdentifier: String = "ProjectCell"
    var WVC = welcomeViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectParser()
//        self.ProjTable.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        
    // Removing all the objects from the canonical_name array. The bug here was, that this array gets repopulated with 280+ more languages every time we navigate back to project page and then to the language page again.
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return projArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as UITableViewCell
        //let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "menuCell")
        cell.textLabel?.text =  projArr[indexPath.row] as NSString
        //cell.detailTextLabel?.text = canArr[indexPath.row] as NSString
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var index = ProjTable!.indexPathForSelectedRow()!.row
        projSelected = pcArr[index] as NSString
        selProjName = projArr[index] as NSString
        navigationController?.popToRootViewControllerAnimated(true)

        //langListReady = false
    }
    
    func projectParser() // Project list was obtained here: http://wikimediafoundation.org/wiki/Our_projects
    {
        if ( projListReady == false)
        {
            if let file = NSBundle(forClass: ProjectController.self).pathForResource("projects", ofType: "json")
            {
                let data = NSData(contentsOfFile: file)
                projJson = JSON(data: data!)
                //println(projJson)
            }
            for (key: String, subJson: JSON) in projJson
            {
                pName = subJson["project_name"]
                pCode = subJson["code"]
                projArr.addObject(pName.description)
                pcArr.addObject(pCode.description)
            }
            //println(projArr)
            dispatch_async(dispatch_get_main_queue(), {
                self.ProjTable!.reloadData()
            })
            projListReady = true
        }
        //println(projArr.count)

    }
    
/*    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        var langSelector: languageController = segue.destinationViewController as languageController
        //var welcome: welcomeViewController = segue.destinationViewController as welcomeViewController
        var tableIndex = ProjTable!.indexPathForSelectedRow()!.row
        var projTempCode = pcArr[tableIndex] as NSString
       // welcome.selectedProj = projTempCode
        langSelector.codeForProj = projTempCode
    }
*/
    
    func manualSegue()
    {
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var navMenu: menuPage = storyboard.instantiateViewControllerWithIdentifier("menuID") as menuPage
        self.presentViewController(navMenu, animated: true, completion: nil)
    }
}