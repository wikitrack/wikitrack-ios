import UIKit


class RevisionsController: UIViewController, UIWebViewDelegate {
    
    var diffMatchPatch: DiffMatchPatch = DiffMatchPatch()
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var webDiff: UIWebView!
    // revId is used to append the fetched revId from the article page to the incomplete API URL to fetch the revisions JSON.
    
    @IBAction func backToList(sender:AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
        println("I'm done here.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webDiff.loadHTMLString(blank, baseURL: nil)

        //println("should work: \(revId)")
        // rcRevisions()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(Bool)
    {
        convertedText.removeAllObjects()
        inlineDiff()
        //println(revId)
        //rcRevisions()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func inlineDiff() /* These references aided in arriving at the right solution to implement 'inline-diff'.
    http://johnmacfarlane.net/pandoc/
        http://regexkit.sourceforge.net/
        http://www.mediawiki.org/wiki/Alternative_parsers
        http://www.mediawiki.org/wiki/Help:Formatting
        https://meta.wikimedia.org/wiki/Help:Wikitext_examples
*/
    {
        var url: NSURL = NSURL(string: newDiffURL + revId)!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description)
        {
            url = NSURL( string: http + projectCode + newDiffURL + revId )!
        }
        else
        {
            url = NSURL( string: http + lCode + projectCode + newDiffURL + revId )!
        }
        let task = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData: NSData = NSData(contentsOfURL:url)!
            let revs: JSON = JSON(data: urlData)
            //            println(revs)//["query"]["pages"]["88277"]["revisions"])[0]["*"])
            rvNew = revs["query"]["pages"]["\(revId)"]["revisions"][0]["revid"].description
            var parentCheck: NSString = revs["query"]["pages"]["\(revId)"]["revisions"][0]["parentid"].description
            if ( parentCheck == "0" )
            {
                println("ParentID:\(parentCheck)")
                println("Warning! This article has only 1 revision id. Please take required measures to redirect user back to titleListViewController.")
                var alert: UIAlertView = UIAlertView()
                let alT = "No Revision"
                let alM = "This article has no revisions/diff to display."
                alert.title = alT
                alert.message = alM
                alert.addButtonWithTitle("OK")
                dispatch_async(dispatch_get_main_queue(),
                    {
                        alert.show()
                        self.dismissViewControllerAnimated(true, completion: nil)
                })
                
            }
            else
            {
                rvOld = revs["query"]["pages"]["\(revId)"]["revisions"][1]["revid"].description
                println(rvNew)
                println(rvOld)
                self.APIparseRev(rvNew)
                self.APIparseRev(rvOld)
            }
            
        })
        task.resume()
        
    }
    
    
    func APIparseRev(var revisionID: NSString)
    {

        diffArray.removeAllObjects()
        var url: NSURL = NSURL( string: parseURL + revisionID)!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description )
        {
            url = NSURL( string: http + projectCode + parseURL + revisionID )!
        }
        else
        {
            url = NSURL( string: http + lCode + projectCode + parseURL + revisionID )!
        }
        var task = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData = NSData(contentsOfURL: url)
            var json = JSON(data: urlData!)
            //println(json)
            //println(json["parse"]["text"]["*"])
            var HTMLstring: NSString = json["parse"]["text"]["*"].description
            
            dispatch_async(dispatch_get_main_queue(),
                {
                    var htmlTOaString: NSAttributedString = self.attributedStringFromHTML(HTMLstring)
                    convertedText.addObject(htmlTOaString.string)
                    if ( convertedText.count > 1 )
                    {
                        //println("html2plain:\(convertedText)")
                        diffArray = self.diffMatchPatch.diff_mainOfOldString(convertedText.objectAtIndex(0).description, andNewString: convertedText.objectAtIndex(1).description)
                        self.diffMatchPatch.diff_cleanupSemantic(diffArray)
                        //println("------------==========\n==============--------------\(diffArray)")
                        var prettyHtml: NSString = self.diffMatchPatch.diff_prettyHtml(diffArray)
                        var stringWithoutPara: NSString = prettyHtml.stringByReplacingOccurrencesOfString("&para;", withString: "")
                        //println(stringWithoutPara)
                        self.webDiff.loadHTMLString(stringWithoutPara, baseURL: nil)
                        println("Diff is ready.")
                    }
                    
            })
        })
        task.resume()
    }
    

    
    // The function is to be used to convert HTML data into plain text. The NSAttributedString method can be run as a 'one-liner' to do the conversion but for our convenience and reusability fator it was decided to convert HTML data from within this method.
    func attributedStringFromHTML( var html: NSString) -> NSAttributedString
    {
        let numberWithInt: UInt = NSUTF8StringEncoding //A UInt constant to represent the Encoding format. Only a 'UInt' can be used.
        
        var options: NSDictionary = [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: numberWithInt] /* Document attributes for interpreting the document cotents. PLEASE NOTE: The NSDocumentTypeDocumentAttribute with a value of NSHTMLTextDocumentType should not be called from a background thread. It will try to synchronize with the main thread, fail and time out. Calling it from the main thread works. */
        
        var attrString: NSAttributedString = NSAttributedString(data: html.dataUsingEncoding(NSUTF8StringEncoding)!, options: options, documentAttributes: nil, error: nil)!
        return attrString
    }
}







/*
    // This function makes use of the swifty JSON library to parse the revision diff from the API url.
    func rcRevisions()
    {
        println(http + lCode + projectCode + rcRevURL + revId)
        var url: NSURL = NSURL(string: http + lCode + projectCode + rcRevURL + revId)
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description )
        {
                url = NSURL( string: http + projectCode + rcRevURL + revId )
        }
        else
        {
            url = NSURL( string: http + lCode + projectCode + rcRevURL + revId )
            println(revId)
        }
        let task = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData: NSData = NSData(contentsOfURL:url)
            let revs: JSON = JSON(data: urlData)
            // println(revs)
            let content: JSON = revs["query"]["pages"]["12443"]["revisions"]
            //println(content)
            for (key: String, subJson: JSON) in revs["query"]["pages"]
            {
                println("Key(PageID): \(key)")
                // println(subJson["revisions"][0]["diff"]["*"])
                diffAsString = subJson["revisions"][0]["diff"]["*"]
                incompleteDiff = diffAsString.description
                incompleteDiff = "<html>\n<body>\n<table>\n" + incompleteDiff + "\n</table>\n</body>\n</html>"
                // println(incompleteDiff)
            }
            
            self.webDiff.loadHTMLString(incompleteDiff, baseURL: nil)
        })
        
        task.resume()
        
    }
*/

