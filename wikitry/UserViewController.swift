import UIKit

class UserViewController: UIViewController
{
    @IBOutlet weak var selUser: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        selUser.addTarget(self, action: Selector(textFieldDidChange()), forControlEvents: UIControlEvents.EditingChanged)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(Bool)
    {
        super.viewDidAppear(true)
        selUser.becomeFirstResponder()
    }
    
    func textFieldDidChange()
    {
        userEntered = (selUser.text.isEmpty) ? false : true
    }
    
    func textFieldShouldReturn ( textField: UITextField ) -> Bool
    {
        if ( textField == selUser )
        {
            println("\nUser name has been entered. Proceed to titleListViewController")
            validUser = selUser.text
            nameHasBeenEntered()
        }
        return true
    }
    
    func nameHasBeenEntered()
    {
        println(validUser)
        //println("desiKalakar")
        if (validUser != "")
        {
                    performSegueWithIdentifier("segueUSERsel", sender: nil)
        }
        else
        {
            var al: UIAlertView = UIAlertView()
            var alTi = "Error"
            var alMe = "Please enter a valid user name to proceed. Thank you."
            al.title = alTi
            al.message = alMe
            al.addButtonWithTitle("OK")
            al.show()
        }
    }
}