//
//  ViewController.swift
//  wikitry
//
//  Created by SI-dev on 08/10/14.
//  Copyright (c) 2014 saaranga. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, rcCallProtocol, UIAlertViewDelegate {


    @IBOutlet var table: UITableView!
    
    @IBAction func backToMenu(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil) //The presenting view controller is responsible for dismissing the view controller it presented. If you call this method on the presented view controller itself, it automatically forwards the message to the presenting view controller.
        // Using a modal or push segue the other way would also present itself as a solution in this case but it would instantiate a new controller.
        println("Click")
    }
    
    
    var rc = rcAPIcall?()
    let kCellIdentifier: String = "TitleCell"
    var revCon = RevisionsController()
    var inNum: NSString = ""
    var codeForURL: NSString = "en"
    
    /*
    var temp: JSON = JSON.nullJSON
    var title: NSMutableArray = []
    let JSON_KEY_query = "query"
    let JSON_KEY_rc: AnyObject! = "recentchanges"
    let JSON_KEY_title = "title"
    */
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //revCon.rcRevisions()
        //self.rc.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
        
        rc = rcAPIcall(delegate: self)
        
        if ( recordValue == menuArray[0].description )
        {
            rc!.apicall()
        }
        else if ( recordValue == menuArray[1].description )
        {
            rc!.userContribCall()
        }
        else if ( recordValue == menuArray[2].description)
        {
            rc!.watchlist()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        switch temp.type {
//        case Type.Array, Type.Dictionary:
//            return temp.count
//        default:
//            return 1
//        }
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as UITableViewCell
        //let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "MyTestCell")
        //println("Honey Singh")
        cell.textLabel?.text =  tableData[indexPath.row] as NSString
        
        return cell
/*
        let cell = tableView.dequeueReusableCellWithIdentifier("JSONParsedByiOSDev", forIndexPath: indexPath) as UITableViewCell
        
        var row = indexPath.row
        
        switch self.temp.type {
        case .Array:
            cell.textLabel?.text = "\(row)"
            cell.detailTextLabel?.text = self.temp.arrayValue.description
        case .Dictionary:
            let key: AnyObject = (self.temp.object as NSDictionary).allKeys[row]
            let value = self.temp[key as String]
            cell.textLabel?.text = "\(key)"
            cell.detailTextLabel?.text = self.temp.description
        default:
            cell.textLabel?.text = ""
            cell.detailTextLabel?.text = self.temp.description
        }
    
      return cell
*/
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var tableIndex = table!.indexPathForSelectedRow()!.row
        revId = revArray[tableIndex].description
        println("tempRev: \(revId)")
        
        if ( revId == "0")
        {
            var pop: UIAlertView = UIAlertView(title: "Apologies", message: "The selected article title doesn't have any revisions that can be displayed.", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "OK")
            pop.show()
        }
        else
        {
            performSegueWithIdentifier("segue.push.rev", sender: nil)
        }
    }
    
    func didReceiveTitle()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.table!.reloadData()
        })
    }
    
    func popVC()
    {
        navigationController?.popViewControllerAnimated(true)
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
//    {
//        var revController: RevisionsController = segue.destinationViewController as RevisionsController
//        var tableIndex = table!.indexPathForSelectedRow()!.row
//        var selectedArticle: NSString = revArray[tableIndex] as NSString
//        revController.revId = selectedArticle
//    }

}