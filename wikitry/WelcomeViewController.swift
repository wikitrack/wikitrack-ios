import UIKit
import Foundation

class welcomeViewController: UIViewController, UIAlertViewDelegate
{
    @IBOutlet var wikitrackLogo: UIImageView!
    @IBOutlet var projButton: UIButton!
    @IBOutlet var langButton: UIButton!
    @IBOutlet var titleImage: UIImageView!
    var logo: UIImage = UIImage(named: WTlogo)!
    var checkCondition: Bool = false
    

/*    @IBAction func projButton(sender: UIButton)
    {
        //performSegueWithIdentifier("segue.choose.proj", sender:nil)
    }
    
    @IBAction func langButton(sender: UIButton) {
    }
*/    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wikitrackLogo.image = logo
        langButton.enabled = false
        projButton.enabled = true
        //langButton.enabled = false
        //wikitrackLogo.image = logo
        // Do any additional setup after loading the view, typically from a nibY.
    }
    
    override func viewWillAppear(Bool)
    {
        welcome()
        // Adding border color to the UIButtons.(Optional)
        /*
        projButton.layer.borderWidth = 0.5
        projButton.layer.borderColor = UIColor(CGColor: )
        projButton.layer.cornerRadius = 1.0
        */
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func welcome()
    {
        //println(projSelected)
        //println(selProjName)
        if (selProjName != "")
        {
//            var alert: UIAlertView = UIAlertView(title: "Remember Project", message: "Would you like the app to remember your selection?", delegate: self, cancelButtonTitle: "Skip", otherButtonTitles: "Remember")
//
//            
//            alert.alertViewStyle = UIAlertViewStyle.Default
//            alert.show()
            
            
            langButton.enabled = true
            projButton.enabled = true
            projButton.setTitle(selProjName, forState: UIControlState.Normal)
            println(selProjName)
            projectCode = projSelected
            println(projectCode)
            langListReady = false //This has been added to allow the language list to be loaded again in the case when, user segues back to proj page from language page and then goes back to language selection page without making any changes to project. Without this condition here, going back to language page would display a blank table as the langListReady would be set to true and hence not allowing the method to parse the required data.
        }
        else
        {
            langButton.enabled = false
            projButton.enabled = true
        }
    }
    
    @IBAction func unwindToWelcome(segue: UIStoryboardSegue) {
        println("Successfully back to Welcome Page")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (buttonIndex == alertView.cancelButtonIndex)
        {
            println("Do not remember selection")
            
        }
        else
        {
            println("Remember selected project")
        }
    }
    
    override func awakeFromNib() {
//        navigationItem.titleView = titleImage
    }
    
//    func projSelected()
//    {
//        self.projButton.setTitle(selectedProj, forState: UIControlState.Normal)
//    }
    
}