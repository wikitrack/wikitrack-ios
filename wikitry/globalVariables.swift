import UIKit


//Variables to hold the name of the images. Application Logo, RC, UI, WL and LI.
var WTlogo: String = "AppLogo"
var imgArr: NSArray = ["Recent_changes", "User_contribution", "Watchlist", "Login"]



var http: NSString = "http://"

// The following variable help in the parsing of the title field from the API.
var recentChangesArticleTitles: NSString = ".org/w/api.php?action=query&list=recentchanges&format=json&continue=&rclimit=20"
var temp: JSON = JSON.nullJSON
var tableData: NSMutableArray = []



// The following variables help in the parsing of the revid from API.
var rcRevURL: NSString = ".org/w/api.php?action=query&prop=revisions&format=json&rvprop=content&rvdiffto=prev&revids="//1226290"
var diffAsString: JSON = JSON.nullJSON
var incompleteDiff: NSString = ""
var noDiffMsg: NSString = "No Revisions to display here. Please select another aritcle on the previous page."
var revArray: NSMutableArray = []
var ID: JSON = JSON.nullJSON
var revId: NSString = ""  // revId is used to append the fetched revId from the article page to the incomplete API URL to fetch the revisions JSON.
//var tempRev: NSString = ""


// Variables for new revisions method
let blank: NSString = "Cooking up the diff...  Thank You for your patience."
var newDiffURL: NSString = ".org/w/api.php?action=query&prop=revisions&format=json&rvprop=ids&rvlimit=2&continue=&pageids="//88277"
//action=query&continue=&prop=revisions&format=json&rvprop=content&rvlimit=2&pageids=88277"
var convertedText: NSMutableArray = []
var diffArray: NSMutableArray = []
var rvNew: NSString = ""
var rvOld: NSString = ""

var parseURL: NSString = ".org/w/api.php?action=parse&format=json&contentmodel=wikitext&mobileformat=&prop=text&oldid="



// These variables are for the menu page.
var menuArray: NSMutableArray = []
var menuJson: JSON = JSON.nullJSON
var tempMenu: JSON = JSON.nullJSON
//var menuTable: NSMutableArray = []
var indexArr: NSMutableArray = []
var recordValue: NSString = ""
let One: Int = 1
let Th: Int = 3
let Fo: Int = 4
var LI: NSString = "Log in"

var logoutURL: NSString = ".org/w/api.php?action=logout"
var UI: NSString = "User Contributions"
var WL: NSString = "Watch List"


//Variables for language selection page
var langJson: JSON = JSON.nullJSON
var canonicalTemp: JSON = JSON.nullJSON
var codeTemp: JSON = JSON.nullJSON
var nameTemp: JSON = JSON.nullJSON
var searchString: NSString = ""
var languageData: NSArray = []
var canArr: NSMutableArray = []
var codeArr: NSMutableArray = []
var nameArr: NSMutableArray = []
var langListReady: Bool = false
var langDataReady: Bool = false

// A global variable to store the selected language code. Cannot be used on language selection page as the indexPathForSelectedRow is not yet identified here. Only after sending the identified language code in another local NSString variable of MenuPage, we can assign the language code to this variable. This was tested many times and it worked for me when the assignment was done within the loadMenu function.
// With the new value successfully assigned to this global variable, it can now be used to be passed as a part of the URL parameter that makes a API request.
var lCode: NSString = ""

// A global variable to store the selected project code.
var projectCode: NSString = ""

// Variables for the project selection page
var projJson: JSON = JSON.nullJSON
var pName: JSON = JSON.nullJSON
var pCode: JSON = JSON.nullJSON
var projArr: NSMutableArray = []
var pcArr: NSMutableArray = []
var projListReady: Bool = false //This Boolean value is used within the projectParser function. The array that store the parsed project names, was observed to be getting project names added to the list every time the view was being loaded and the function was being called. Thus, resulting in a continual increase in the size of the array over the usage of the app. This has been put to check using this boolean value.

// Variables for the welcome page.
var projSelected: NSString = ""
var selProjName: NSString = ""


// Variables for the User contribution method
var ucURL: NSString = ".org/w/api.php?action=query&continue=&list=usercontribs&format=json&uclimit=10&ucuser="
var ucTemp: JSON = JSON.nullJSON
var timeTemp: JSON = JSON.nullJSON
var ucRevTemp: JSON = JSON.nullJSON
var ucTitle: NSMutableArray = []
var ucTimeStamp: NSMutableArray = []
var ucRevID: NSMutableArray = []
var correctSelection: Bool = false

//Variables for the username selection ViewController.
var userEntered: Bool = false
var validUser: NSString = ""



// Variables for the login page.
var loginURL: NSString = ".org/w/api.php?action=login&format=json"
let paraName: NSString = "&lgname="
let paraPassword: NSString = "&lgpassword="
let paraToken: NSString = "&lgtoken="
var lgname: NSString = ""
var lgpassword: NSString = ""
var lgtoken: NSString = ""
var sessionID: NSString = ""
var textEntered: Bool = false
var attemptAtLg: Bool = false
var token: JSON = JSON.nullJSON
var resultJson: JSON = JSON.nullJSON
var loggedIN: Bool = false



// Variables for the watchlist method
var watchlistURL: NSString = ".org/w/api.php?action=query&continue=&list=watchlist&format=json"
//var wlRev: NSMutableArray = []
var wlTitle: NSMutableArray = []
var wlJSONtitle: JSON = JSON.nullJSON
var wlJSONrev: JSON = JSON.nullJSON



//User data collection variables

let token_url = "http://wikitrack.hpnadig.net/services/session/token"
let login_url = "http://wikitrack.hpnadig.net/api/user/login"
let node_url = "http://wikitrack.hpnadig.net/api/node"
let usrname = "qwerty"
let pwd = usrname

