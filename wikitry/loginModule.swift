import UIKit

protocol SuccessProtocol
{
    func handleLogin()
}

class loginModule  // References: https://meta.wikimedia.org/wiki/Schema:MobileWikiAppLogin
{
    var delegate: SuccessProtocol
    init(delegate: SuccessProtocol)
    {
        self.delegate = delegate
    }
    
    //let loginVC: LoginViewController = LoginViewController()
    func loginRequest()
    {
        println(lgname)
        if ( lgname != "" && attemptAtLg == false )
        {
            var request: NSMutableURLRequest = NSMutableURLRequest( URL: NSURL( string: loginURL + paraName + lgname + paraPassword + lgpassword )!)
            if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description )
                // COMMONS, META-WIKI, INCUBATOR & WIKISPECIES
            {
                request = NSMutableURLRequest( URL: NSURL( string: http + projectCode + loginURL + paraName + lgname + paraPassword + lgpassword )!)
            }
            else
            {
                request = NSMutableURLRequest( URL: NSURL ( string: http + lCode + projectCode + loginURL + paraName + lgname + paraPassword + lgpassword )!)
            }
            
            request.HTTPMethod = "POST"
            //println(request)
            var task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {data, response, error in
                var json: JSON = JSON(data: data)
                println("JSON when request is 'needToken' \n\(json)")
                token = json["login"]["token"]
                var tempSession: JSON = json["login"]["sessionid"]
                println("Token value: \(token)")
                lgtoken = token.description
                sessionID = tempSession.description
                self.loginTokenOp() //Calling the tokenRequestMethod here itself so that, the token value is obtained and only then the method called. Anything outside of 'task.resume()' will be executed before dataTaskWithRequest has been completed.
                //println(" The token has been retrieved sucessfully, the token and sessionID are: \n\(lgtoken) \n\(sessionID) \nrepectively.")
            })
            task.resume()
            attemptAtLg = true
        }
        
        //attemptAtLg = true
        
    }
    
    func loginTokenOp()
    {
        println(lgtoken) // continue work from here. lgtoken is being passed as blank here and for some reason this function is getting executed before 'loginRequest'. Please look into this next.
        if ( lgtoken != "" )
        {
            var requestWithToken: NSMutableURLRequest = NSMutableURLRequest( URL: NSURL ( string: loginURL + paraName + lgname + paraPassword + lgpassword + paraToken + lgtoken )! )
            if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description )
            {
                requestWithToken = NSMutableURLRequest( URL: NSURL ( string: http + projectCode + loginURL + paraName + lgname + paraPassword + lgpassword + paraToken + lgtoken )!)
            }
            else
            {
                requestWithToken = NSMutableURLRequest(URL: NSURL ( string: http + lCode + projectCode + loginURL + paraName + lgname + paraPassword + lgpassword + paraToken + lgtoken )!)
            }
            requestWithToken.HTTPMethod = "POST"
            //println(requestWithToken)
            var taskWithToken = NSURLSession.sharedSession().dataTaskWithRequest(requestWithToken, completionHandler: {data, response, error in
                
                resultJson = JSON(data: data)
                //println(response)
                println(resultJson)
                self.completionBlock()
            })
            taskWithToken.resume()
        }
        
    }
    
    func completionBlock()
    {
        var loginStatus = resultJson["login"]["result"]
        println(loginStatus)
        var user = resultJson["login"]["lgusername"]
        if ( loginStatus == "Success" )
        {
            loggedIN = true // boolean value is being used here to identify if user is logged in. Also this value will be used to change the table cell value once the user has logged in.
            
            var alert: UIAlertView = UIAlertView()
            var alTitle = "Done"
            var alMessage = "Login successful. You are logged in as \(user)."
            alert.title = alTitle
            alert.message = alMessage
            alert.addButtonWithTitle("OK")
            
            getCSRF_postData()
            
            //println("Now pop back to menuPage with the username and display the watchlist.")
            dispatch_async(dispatch_get_main_queue(), {
                alert.show()
                self.delegate.handleLogin()
                //self.mVC.handleLogin()
                //self.dismissViewControllerAnimated(true, completion:nil)  http://stackoverflow.com/questions/24038713/syntax-of-block-in-swift
                //                        { () -> Void in
                //                    self.mVC.handleLogin() })
            })
            
            //self.presentViewController(menuPage(), animated: true, completion: self.MP.handlingLogin)
        }
        else
        {
            var alert: UIAlertView = UIAlertView()
            var alTitle = "Error"
            var alMessage = "Login unsuccessful. Please try again with valid username and password."
            alert.title = alTitle
            alert.message = alMessage
            alert.addButtonWithTitle("OK")
            dispatch_async(dispatch_get_main_queue(),
                {
                    alert.show()
            })
            attemptAtLg = false
        }
    }
    
    // The following functions are part of a single group and can be placed in a separate class at a later stage in this app's development.
    // After moving these functions to a different class we can perform userData collection else where in the app. 
    // The other most likely place for this would be after logged-in user changes the selected proj and language. So at the langViewController we can check if 'lgname' is non empty in which case we can collect this user data.
    // The getCSRF_postData function handles collection and storing of user data such as username, selected project and language. This data is saved on 'wikitrack.hpnadig.net'.
    func getCSRF_postData()
    {
        var url: NSURL = NSURL(string: token_url)!
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData: NSData = NSData(contentsOfURL: url)!
            //println(urlData)
            var string: NSString = NSString(data: urlData, encoding: NSUTF8StringEncoding)!
            println(string)
            self.loginToPostData(string)
        })
        task.resume()
        
        
    }
    
    func loginToPostData(token: NSString){
        var request: NSMutableURLRequest = NSMutableURLRequest( URL: NSURL( string: login_url)!)
        var parameter: NSDictionary = [ "username": usrname, "password": pwd ]
        
        request.HTTPMethod = "POST"
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(parameter, options: nil, error: nil)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "X-CSRF-Token")
        //println(request)
        var task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {data, response, error in
            var json: JSON = JSON(data: data)
            println(json)
            println("\n\n\(token)")
            self.postData(token)
        })
        task.resume()
    }
    
    
    func postData(CSRF: NSString){
        var request: NSMutableURLRequest = NSMutableURLRequest( URL: NSURL( string: node_url)!)
        
        request.HTTPMethod = "POST"
        request.HTTPBody = createJSON()
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(CSRF, forHTTPHeaderField: "X-CSRF-Token")
        
        var task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {data, response, error in
            var json: JSON = JSON(data: data)
            println("Post data\(json)")
        })
        task.resume()
    }

    func createJSON() -> NSData
    {
        var d1: NSMutableDictionary = ["value":lgname]
        var d2: NSMutableDictionary = ["value":selProjName]
        var d3: NSMutableDictionary = ["value":lCode]
        var arr1: NSArray = [d1]
        var arr2: NSArray = [d2]
        var arr3: NSArray = [d3]
        var und1: NSDictionary = ["und": arr1]
        var und2: NSDictionary = ["und": arr2]
        var und3: NSDictionary = ["und": arr3]
        var jsonDict: NSDictionary = ["type":"wikitrack", "title":"iOS", "field_user_name": und1,"field_project_name": und2, "field_language": und3]
        var jsonData: NSData = NSJSONSerialization.dataWithJSONObject(jsonDict, options: nil/*NSJSONWritingOptions.PrettyPrinted*/, error: nil)!
        var str:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
        println(str)
        
        return jsonData
    }
    

}