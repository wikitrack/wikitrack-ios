import UIKit

protocol rcCallProtocol
{
    func didReceiveTitle()
    func popVC()
}

class rcAPIcall
{

    var delegate: rcCallProtocol
    
    init(delegate: rcCallProtocol)
    {
        self.delegate = delegate
    }
    
//    This function is asking for some changes.
    func apicall()
    {
        revArray.removeAllObjects() //Clearing the revArray as it stores the revids for more than one method.
        tableData.removeAllObjects()
        //println(recentChangesArticleTitles)
        println(lCode)
        println(http + lCode + projectCode + recentChangesArticleTitles)
        var url: NSURL = NSURL( string: http + lCode + projectCode + recentChangesArticleTitles )!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description) //COMMONS, META-WIKI, INCUBATOR, WIKISPECIES do not need the laguage code in their API request URL.
        {
            url = NSURL( string: http + projectCode + recentChangesArticleTitles )!
            println(url)
        }
        else
        {
            url = NSURL(string: http + lCode + projectCode + recentChangesArticleTitles)!
        }
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error in
            /* Recent changes API JSON was parsed manually in the section. This parse was discarded as SwiftyJSON library is now being used.
            The library makes to parsing simple and, the JSON easier to comprehend.
            var jsonErrorOptional: NSError?
            
            let jsonOptional: AnyObject! = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(0), error: &jsonErrorOptional)
            
            //            if let json = jsonOptional as? Dictionary<String, AnyObject> {
            if let json = jsonOptional as? NSDictionary {
            println(json)
            var title: NSMutableArray = []
            var query: NSDictionary = json.objectForKey(self.JSON_KEY_query) as NSDictionary
            var rc: NSMutableArray = query.objectForKey(self.JSON_KEY_rc) as NSMutableArray
            var i = 0
            for i in i...9 {
            var item: NSDictionary = rc.objectAtIndex(i) as NSDictionary
            //print(item)
            var temp_title: NSString = item.objectForKey(self.JSON_KEY_title) as NSString
            println(temp_title)
            title.addObject(temp_title)
            
            }
            var x = 0
            for x in x...9 {
            println(title.objectAtIndex(x))
            }
            */
            
            let urlData = NSData(contentsOfURL: url)
            let json = JSON(data: urlData!)
            println(json)
            
            var title: NSMutableArray = []
            //            var i = 0
            //            for i in i...9
            //            {
            //                temp = json["query"]["recentchanges"][i]["title"]
            //                println(temp)
            //                title.addObject(temp.description)
            //            }
            
            for (key: String, subJson: JSON) in json["query"]
            {
                println("KEY \(key)")
                println(subJson.arrayValue.count)
                var index = 0
                for index in index..<subJson.arrayValue.count
                {
                    // This part will fetch the title of the article from the array listed out under the 'recentchanges' key
                    //println(subJson[index]["title"])
                    temp = subJson[index]["title"]
                    title.addObject(temp.description)
                    
                    // This part will fetch the corresponding revid for that article
                    //println(subJson[index]["revid"])
                    ID = subJson[index]["pageid"]
                    revArray.addObject(ID.description)
                }
                
            }
            
            tableData = title
            self.delegate.didReceiveTitle()
//            dispatch_async(dispatch_get_main_queue(), {
//                tableData = title
//                self.table!.reloadData()
//            })
            
            
        })
        task.resume()
    }
    
    
    // This function fetches user contributions for a specified user.
    func userContribCall()
    {
        revArray.removeAllObjects() //Clearing the revArray as it stores the revids for more than one method.
        ucTitle.removeAllObjects()
        tableData.removeAllObjects()
        var url: NSURL = NSURL( string: http + lCode + projectCode + ucURL )!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description) //COMMONS, META-WIKI, INCUBATOR, WIKISPECIES do not need the laguage code in their API request URL.
        {
            url = NSURL( string: http + projectCode + ucURL + validUser)!
            println(url)
        }
        else
        {
            url = NSURL(string: http + lCode + projectCode + ucURL + validUser)!
            println(url)
        }

        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData: NSData = NSData(contentsOfURL: url)!
            let json: JSON = JSON(data: urlData)
            println(json)
            correctSelection = true
            for ( key: String, subJson: JSON) in json["query"]
            {
                println("keyUC: \(key)")
                println(subJson.arrayValue.count)
                if (subJson.arrayValue.count == 0 )
                {
                    //tableData.removeAllObjects()   // Clearing the TableData Array as the user has probably looking for the contributions in the wrong project or language.
                    
                    var alert: UIAlertView = UIAlertView()
                    var aTitle = "Nothing to display here."
                    var aMSG = "No contributions have been made under " + selProjName + " in the selected language."
                    alert.title = aTitle
                    alert.message = aMSG
                    alert.addButtonWithTitle("OK")
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            alert.show()
                    })
                }
                else
                {
                    var index = 0
                    for index in index..<subJson.arrayValue.count
                    {
                        ucTemp = subJson[index]["title"]
                        ucTitle.addObject(ucTemp.description)
                        
                        ucRevTemp = subJson[index]["pageid"]
                        revArray.addObject(ucRevTemp.description)
                    }
                }
            }
            tableData = ucTitle
            self.delegate.didReceiveTitle()
            
        })
        task.resume()
        
        
    }
    
    //This function will fetch a list of watchlist article titles for a logged in user. It will not work if user is not logged in.
    func watchlist()
    {
        revArray.removeAllObjects()
        wlTitle.removeAllObjects()
        tableData.removeAllObjects()
        var url: NSURL = NSURL( string: watchlistURL)!
        if ( selProjName == projArr[8].description || selProjName == projArr[9].description || selProjName == projArr[10].description || selProjName == projArr[11].description ) //Commons, Metawiki, Incubator, Wikispecies do not need the language code in their API request URL.
        {
            url = NSURL( string: http + projectCode + watchlistURL )!
        }
        else
        {
            url = NSURL( string: http + lCode + projectCode + watchlistURL )!
        }

        let task = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {data, response, error in
            let urlData: NSData = NSData(contentsOfURL: url)!
            let WLJSON: JSON = JSON(data: urlData)
            println(WLJSON)
            
            if WLJSON["error"]
            {
                //tableData.removeAllObjects()
                var alert:UIAlertView = UIAlertView()
                var alTi = "Not logged-in"
                var alMe = "You must be logged-in to view watchlist. Please login first."
                alert.title = alTi
                alert.message = alMe
                alert.addButtonWithTitle("OK")
                dispatch_async(dispatch_get_main_queue(),
                    {
                        alert.show()
                        self.delegate.popVC()
                    })

            }
            else
            {
                for (key: String, subJson: JSON) in WLJSON["query"]
                {
                    
                    var title: NSMutableArray = []
                    var rev: NSMutableArray = []
                    println("KEY: \(key)")
                    println(subJson.arrayValue.count)
                    if ( subJson.arrayValue.count == 0)
                    {
                        tableData.removeAllObjects()
                        
                        var alert:UIAlertView = UIAlertView()
                        var alTi = "Watchlist Empty"
                        var alMe = lgname + " doesn't have any articles in the watchlist for " + selProjName + " in the selected language."
                        alert.title = alTi
                        alert.message = alMe
                        alert.addButtonWithTitle("OK")
                        dispatch_async(dispatch_get_main_queue(),
                            {
                                alert.show()
                            })
                    }
                    else
                    {
                        var index = 0
                        for index in index..<subJson.arrayValue.count
                        {
                            wlJSONtitle = subJson[index]["title"]
                            wlTitle.addObject(wlJSONtitle.description)
                            
                            wlJSONrev = subJson[index]["pageid"]
                            revArray.addObject(wlJSONrev.description)
                        }
                        println("\n\n \(wlTitle)")
                    }
                }
            }
            tableData = wlTitle
            self.delegate.didReceiveTitle()
        })
        task.resume()
    }
    
}